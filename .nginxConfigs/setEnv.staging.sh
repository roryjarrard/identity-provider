#!/bin/bash

export APP_TIMEZONE=America/Toronto;
export APP_ENV=staging;
export APP_URL="https://identity-staging.cardataconsultants.com";
export APP_KEY=c304229f956a0c38768a769c819bcbd8;

export DB_CONNECTION=cardata;
export DB_DRIVER=mysql;
export DB_HOST=10.232.2.142;
export DB_PORT=3306;
export DB_DATABASE=cd_master;
export DB_USERNAME=cduser;
export DB_PASSWORD=CarData01;
export DEFAULT_DB=cd_master;

export MAIL_DRIVER=smtp;
export MAIL_HOST=smtp.sendgrid.net;
export MAIL_PORT=587;
export MAIL_USERNAME=cardatadevelopment;
export MAIL_PASSWORD=cardata01;
export MAIL_ENCRYPTION=tls;
export MAIL_FROM_NAME="CarData Support";
export MAIL_FROM_ADDRESS=support@cardataconsultants.com;
export RESTRICT_EMAIL=false;
export QA_EMAIL=egriffin@cardataconsultants.com;

export QUEUE_DRIVER=database;
export JWT_SECRET=Lhi3qnWfmxx4tHW2dxFoj44A46gusGzHanBMxVoAbHMOKf8wWQXovtyce3qyWMpa;
export JWT_TTL=20160;
export TRACKING_SECRET=xjkIAU3ns918;
export PASSPORT_EXP_DAYS=30;

export CIPHER_METHOD="AES-256-CBC";
export CIPHER_KEY=eKXP34x3maP7;
export CIPHER_IV=ycww6erX8499XAvE;

export CDO_URL="https://portal-staging.cardataconsultants.com";
export PASSWORD_RESET_HOST="https://portal-staging.cardataconsultants.com";
export RULES_URL="https://rules-staging.cardataconsultants.com";
export SUPPORT_RESOURCE="https://client-support-staging.cardataconsultants.com";
export MAIL_SERVICE_URL="https://mail-service-staging.cardataconsultants.com";
