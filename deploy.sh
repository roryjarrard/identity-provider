#!/usr/bin/env bash

ENVIRONMENT=$1
MFILE='deploy.identity.tgz'

if [[ "$ENVIRONMENT" = 'staging' ]]; then
    SSH_USER='cd-staging'
elif [[ "$ENVIRONMENT" = 'prod' ]]; then
    SSH_USER='api-prod'
elif [[ "$ENVIRONMENT" = 'qa' ]]; then
    SSH_USER='id-qa'
fi

POST_SCRIPT="post_deploy.sh"

tar --exclude=.git -zcf ${MFILE} app/ bootstrap/ config/ public/ resources/ routes/ storage/ vendor/ artisan .nginxConfigs/setEnv.${ENVIRONMENT}.sh .nginxConfigs/identity.${ENVIRONMENT}.conf
scp ${MFILE} ${SSH_USER}:.
rm ${MFILE}
ssh -tt ${SSH_USER} 'bash -s' < ${POST_SCRIPT} ${ENVIRONMENT}
