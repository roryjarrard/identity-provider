#!/usr/bin/env bash

ENVIRONMENT=$1
DATE=$(date +%F.%H.%M)
FOLDER="dev.$DATE"
MPATH='/var/www/html/identity-provider'
PROJECTDIR="$MPATH/$FOLDER"
MFILE='deploy.identity.tgz'
OWNER='nginx:nginx'

sh -c "sudo mkdir -p $MPATH/$FOLDER"
sh -c "sudo mv $MFILE $PROJECTDIR/"
sh -c "cd $PROJECTDIR && sudo tar -zxf $MFILE"
sh -c "cd $PROJECTDIR && sudo rm $MFILE"
sh -c "cd $PROJECTDIR && sudo mv .nginxConfigs/setEnv* setEnv.sh"
sh -c "cd $PROJECTDIR && sudo mv .nginxConfigs/identity.*.conf /etc/nginx/conf.d/identity.conf"
sh -c "cd $PROJECTDIR && sudo rm -Rf .nginxConfigs"
sh -c "cd $PROJECTDIR && sudo chown -R $OWNER ."
sh -c "cd $PROJECTDIR/storage && sudo ln -sTf /var/www/oauth-keys/oauth-private.key oauth-private.key"
sh -c "cd $PROJECTDIR/storage && sudo ln -sTf /var/www/oauth-keys/oauth-public.key oauth-public.key"
sh -c "cd $MPATH && sudo ln -sTf \$(readlink current) previous"
sh -c "cd $MPATH && sudo ln -sTf $FOLDER current"
sh -c "cd $MPATH && sudo chown -R $OWNER ."
sh -c "cd $MPATH && ls -t | tail -n +6 | sudo xargs rm -rf --"
sh -c "cd /var/www/logs/identity-provider && ls -t | tail -n +8 | sudo xargs rm -rf --"
sudo chown root:root /etc/nginx/conf.d/identity.conf
if [[ "$ENVIRONMENT" = 'staging' ]]; then
    sudo systemctl restart php-fpm && sudo systemctl restart nginx && exit
else
    sudo systemctl restart nginx.service && exit
fi
exit
