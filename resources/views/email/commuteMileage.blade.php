<h1>Hello Dev Team,</h1>

<p>An error was encountered when calculating commuter mileage:</p>

<p>
    {{ $error }}
</p>

<p>
    Regards,<br>

    {{ config('mail.from')['name'] }}
</p>
