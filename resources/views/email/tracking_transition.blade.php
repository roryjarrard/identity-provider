<div style='font-family: "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";'>
<h2>Hello {{ $user->first_name }}!</h2>

<p>We are excited to announce that {{ $company_name }}. has joined our Mi-Route Tracking program! Your CarData Driver account provides you with access to the Mi-Route APP. Installing and using Mi-Route in your device helps you track your trips and business miles.</p>

<p>Mi-Route tracks your trips and mileages using location-based tracking services offered by Google Maps.  Your employer requested this service to ease your access for business mileage classification.</p>

<p>Mi-Route uses your information to calculate your vehicle expense reimbursement submissions to your employer.</p>

<p>Your username is <strong>{{ $user->username }}</strong>. If you forgot your password you may reset it in the application.

    Here are some basic instructions for downloading and beginning to use the application:</p>

<div style="padding: 20px; border: 1px solid #aaddee; display: inline-block; border-radius: 10px;">
    <h3>Installation</h3>
    <p>Mi-Route is available for download in both the iOS and Google Play stores.</p>

    <h3>Use</h3>

    <ul style="list-style: none; line-height: 1.4">
        <li>Login with your username ({{ $user->username }}) and password</li>
        <li>Once you are logged in you will be brought to the dashboard
            <ul>
                <li>Tap &ldquo;Enable Tracking&rdquo; to begin tracking
                    <ul>
                        <li>You will be prompted to:
                            <ul>
                                <li>Android
                                    <ul>
                                        <li>Allow Bluetooth access if prompted</li>
                                        <li>Enable location access</li>
                                    </ul>
                                </li>
                                <li>Apple
                                    <ul>
                                        <li>Allow Bluetooth access if prompted</li>
                                        <li>Allow motion and activity access</li>
                                        <li>Allow location access
                                            <ul>
                                                <li>Select &ldquo;Always Allow&rdquo;</li>
                                                <li>If you are on iOS 13, and &ldquo;Always Allow&rdquo; does not
                                                    appear, go
                                                    to Settings &rarr; Mi-Route &rarr; Location &rarr; Select Always
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>If you deny any of these requests, or incorrectly configure your permissions, the app will
                            let
                            you know and prompt you to fix them (on iOS there will be an alert telling you to open
                            settings)
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>Go about making your trips
            <ul>
                <li>Open Mi-Route daily (or every other day) to ensure that the tracker is not stopped in the background
                    <ul>
                        <li>Automatic updates, etc. can cause the tracker to be turned off overnight</li>
                    </ul>
                </li>
                <li>You can do other things on your phone at this time, but do not completely close Mi-Route
                    <ul>
                        <li>Putting the app to the background is OK
                            <ul>
                                <li>On iPhones < 10
                                    <ul>
                                        <li>Tap the home button</li>
                                    </ul>
                                </li>
                                <li>On iPhones >= 10
                                    <ul>
                                        <li>Swipe up from the bottom, select another tab or the home screen (don't fully
                                            swipe the application upwards)
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>To disable tracking go to the dashboard and click the <strong>Disable Tracking</strong> button at the top of
            the
            screen
            <ul>
                <li>
                    If you do not disable tracking but close the app Mi-Route will still record trips. Accuracy however,
                    will be greatly reduced.
                </li>
            </ul>
        </li>
    </ul>
</div>

<p>By using Mi-Route, you agree this information will be recorded in your CarData Driver account and shared with your employer to facilitate your vehicle reimbursements. If you do not want this information to be collected and shared as described, please disable the tracking feature.</p>

<p>Should you have any questions or issues please reach out to your CSA:</p>

<p style="font-weight: bold;">{{ $support->firstName . ' ' . $support->lastName }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$support->email}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{
    $support->phone }}</p>

<p>Thank you, and we hope you enjoy using the Mi-Route application!</p>
</div>