Welcome {{ $user['first_name'] }},
<br><br>
We have created a{{ $user['type'] == 'admin' ? 'n' : '' }} {{ $user['type'] }} account for you to access CarData Online, and the Mi-Route Mileage Tracking Application.
<br><br>
Your new username is <span style="font-weight: bold; text-decoration: underline;">{{ $user['username'] }}</span>
<br><br>

    <h2>STEP 1: YOUR PERSONAL PROFILE REGISTRATION</h2>
    <p>
        To login, and complete you registration, go to <a href="{{ $cdo_url }}/resetPassword.php?{{ $link }}">Set Password</a> to set your initial password. You will then be redirected to
        the login page. Enter your username and password there, and login.
        <br><br>
        Edit/Update and save your pages as you navigate through the completion of your Personal Profile Registration.  As you move through the site, here are a few tips to help with your set-up process:
        <br><br>
        You will be asked to provide your vehicle data, so please have your Make, Model, Vehicle Year, @if ($company_plan == 'favr') original MSRP sticker price, @endif and Current odometer available.
        <br><br>
        Once the registration is complete, you are eligible to download the app and begin tracking your business mileage.
        Mileage tracking for reimbursement will begin on your CarData start date of {{ $start_date }}.  Mileage prior to your CarData start date will not be recorded as mileage for reimbursement.
    </p>
    <h2>STEP 2: Mi-ROUTE MILEAGE TRACKING APP</h2>
    <p>
        The Mi-Route Mileage Tracking Power Point Instructions can be found by clicking on the information button for Mi-Route on the top right of your screen.
        <br><br>
        By using Mi-Route, you agree this information will be recorded in your CarData Driver account and shared with your employer to facilitate your vehicle reimbursements. If you do not want this information to be collected and shared as described, please disable the tracking feature.
        <br><br>
    </p>


Should you have any questions or issues please reach out to your CSA:
<br><br>
{{ $support->firstName . ' ' . $support->lastName }} at {{ $support->email }} or {{ $support->phone }}
<br><br>
Thank you, and welcome to the {{ $company_name }} Vehicle Reimbursement Program, administrated by CarData Consultants.

