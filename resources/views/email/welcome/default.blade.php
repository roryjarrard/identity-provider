Welcome {{ $user['first_name'] }},
<br><br>
We have created a{{ $user['type'] == 'admin' ? 'n' : '' }} {{ $user['type'] }} account for you to access CarData Online.
<br><br>
Your new username is <span style="font-weight: bold; text-decoration: underline;">{{ $user['username'] }}</span>
<br><br>
@if($user['type'] == 'driver' && $miroute)
    <h2>STEP 1: YOUR PERSONAL PROFILE</h2>
    <p>
        To login, go to <a href="{{ $cdo_url }}/resetPassword.php?{{ $link }}">Set Password</a> to set your initial password. You will then be redirected to
        the login page. Enter your username and password there.
        <br><br>
        Then save the pages as you go through tem, to complete your Personal Profile setup.
    </p>
    <h2>STEP 2: Mi-ROUTE MILEAGE APP</h2>
    <p>
        Mi-Route is a mileage app for you to easily record your business mileage and trips.
        <br><br>
        Once inside CarData Online, click on the &ldquo;Support&rdquo; tab. You will see support contact information useful to you, and below that in the tutorials section, a Mi-Route video tutorial. The video has helpful information on how to download and install Mi-Route, and how to use it to enter and edit trip information.
        <br><br>
        Until your CarData System Start Date, your access to the Mi-Route platform will be in DEMO mode.
        Please take the opportunity to download the app, and to familiarize yourself with Mi-Route in the DEMO mode.
        Your actual business mileage trips will begin on your CarData System Start Date.
    </p>
    <br><br>
@else
    To login, navigate to To login, go to <a href="{{ $cdo_url }}/resetPassword.php?{{ $link }}">Set Password</a> to set your initial password. You will then be redirected to
    the login page. Enter your username and password there.
    <br><br>

    @if ($company_plan == 'favr')
    Once inside CarData Online click on the &ldquo;Support&rdquo; tab. You will see support contact information useful to you, and below that &ldquo;CarData Online Tutorials&rdquo;. The Tutorial will show you how to set up your account and how to use CarData Online. It is important to view the Tutorial before entering any data. The Tutorial is approximately 3 and a half minutes in length.
    <br><br>
    @endif
@endif

Please contact {{ $support->firstName . ' ' . $support->lastName }} at {{ $support->email }} or {{ $support->phone }} if you have any questions.

