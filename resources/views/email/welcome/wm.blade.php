We have created an account for you to access your CarData information. Your new username is<br>
<span style="font-weight: bold; text-decoration: underline;">{{ $user['username'] }}</span>
<br><br>
To login, go to <a href="{{ $cdo_url }}/resetPassword.php?{{ $link }}">Set Password</a> to set your initial password. You will then be redirected to the login page. Enter your username and password.
<br><br>
@if ($company_plan == 'favr')
    Once inside CarData Online click on the &ldquo;Support&rdquo; tab. You will see support contact information useful to you, and below that &ldquo;CarData Online Tutorials&rdquo;. The Tutorial will show you how to set up your account and how to use CarData Online. It is important to view the Tutorial before entering any data. The Tutorial is approximately 3 and a half minutes in length.
    <br><br>
@endif

As you move through the site, here are a few tips to help with your set-up process:
<br><br>
<ul>
<li>When you fill in your personal info, be sure to &ldquo;save&rdquo; your information before moving to the next step-- you will need to do this on each page</li>
<li>You will be asked to provide your vehicle data, so please have that available when you log in</li>
</ul>
<br>
Your set-up process is complete when you reach the &ldquo;Mileage Entry&rdquo; page. If you do not reach the &ldquo;Mileage Entry&rdquo; page, your set-up is not complete.
<br><br>
If you have any questions about the set-up process or the transition to CarData, please contact {{ $support->firstName . ' ' . $support->lastName }} at {{ $support->email }} or {{ $support->phone }}.
<br><br>
