{{ $user['first_name'] }}
<br><br>
We have created a{{ $user['type'] == 'admin' ? 'n' : '' }} {{ ucwords($user['type']) }} account for you to access CarData Online.  Your new username is<br>
<span style="font-weight: bold; text-decoration: underline;">{{ $user['username'] }}</span>
<br><br>
To login, go to <a href="{{ $cdo_url }}/resetPassword.php?{{ $link }}">Set Password</a> to set your initial password. You will then be redirected to
the login page. Enter your username and password there.
<br><br>
@if ($user['type'] == 'driver')
Once inside CarData Online you will see an Information button with the words &ldquo;Video Tutorial&rdquo;. The Tutorial will show you
you how to set up your account and how to use CarData Online. It is important to view the Tutorial before entering any data. The
Tutorial is constructed of multiple modules, each about one minute in length.
<br><br>
@endif
If you have any questions, you can contact {{ $support->firstName . ' ' .$support->lastName }} at {{ $support->email }} or {{ $support->phone }}.


