<h2>Hello {{ $first_name }},</h2>

@if($use_coordinator)
<p>
    The password for your <strong>{{ $users[0]['type'] }}</strong> account on CarData Online was reset by an Admin.
</p>
@endif
<p>
    Follow the link below to reset your CarData password(s) for the following users:
</p>


<table>
    <thead>
    <tr>
        <th style="width: 200px; text-align: left;">Username</th>
        <th style="width: 150px; text-align: left;">Full Name</th>
        <th style="width: 100px; text-align: left;">User Type</th>
        <th style="width: 200px; text-align: left;">Company</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{ $user['username'] }}</td>
            <td>{{ $user['first_name'] }} {{ $user['last_name'] }}</td>
            <td>{{ ucwords($user['type']) }}</td>
            <td>{{ $user['company_name'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>


<p>
    <a href="{{ $host }}/resetPassword.php?{{ $link }}">Reset Password</a>
</p>

<p>
    This link will expire in 5 days.
</p>

<p>
    For future reference, or if this link has expired, you can always navigate to {{ env('PASSWORD_RESET_HOST') }}<br>
    and click on the <strong>&ldquo;Forgot Username or Password&rdquo;</strong> link to reset your password at any time.
</p>

<p>
    Regards,<br>

    {{ $support->firstName . ' ' . $support->lastName }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $support->phone }}
</p>
