<h2>{{ $pulteAdmin->first_name . ' ' . $pulteAdmin->last_name }}</h2>

<p>
    Please be advised that the file transfer of {{ $confirmationDetails['runDate'] }} has generated the following message:
</p>

<h3>Summary</h3>
<table>
    <thead>
        <tr>
            <th>Action</th>
            <th style="text-align: right">Count</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>Added</td>
            <td style="text-align: right">{{ $confirmationDetails['addedRows'] }}</td>
        </tr>
        <tr>
            <td>Terminated</td>
            <td style="text-align: right">{{ $confirmationDetails['terminatedRows'] }}</td>
        </tr>
        <tr>
            <td>Errors</td>
            <td style="text-align: right">{{ $confirmationDetails['errorRows'] }}</td>
        </tr>
        <tr>
            <td>To be Terminated</td>
            <td style="text-align: right">{{ $confirmationDetails['toBeTerminated'] }}</td>
        </tr>
        <tr>
            <td>Deactivated Managers</td>
            <td style="text-align: right">{{ $confirmationDetails['deactivated'] }}</td>
        </tr>
        <tr>
            <td>Reinstated Managers</td>
            <td style="text-align: right">{{ $confirmationDetails['reactivated'] }}</td>
        </tr>
    </tbody>
</table>

<h3>Details</h3>
<p style="margin-top: 0">
    =============================<br>
    {!! $confirmationDetails['logDetails'] !!}
</p>



