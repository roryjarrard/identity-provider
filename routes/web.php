<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Request;

$router->get('/', function () use ($router) {
    // ]dd(config('queue.default'));
    return 'identity-provider ' . $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('login', 'AuthController@login');
    $router->post('deleteAccessTokens', 'AuthController@deleteAccessTokens');
});
$router->post('logout', 'AuthController@logout');

$router->get('/test', function (Request $request) {
    return response()->json(['message'=>'success'], 200);
});

$router->get('/health/ping', function (Request $request) {
    return response()->json(['name' => "identity-provider", 'timestamp' => time(), 'datetime' => date("Y-m-d H:i:s")], 200);
});

$router->group(['middleware' => ['client'], 'prefix' => 'api'], function () use ($router) {
    $router->get('/getUserInfo[/{driver_id}]', 'AuthController@getUserInfo');
    $router->get('/getCarDataAdminInfo', 'AuthController@getCarDataAdminInfo');
    $router->get('/getClientInfo[/{client_id}]', 'AuthController@getClientInfo');
    $router->get('/testOauth', 'AuthController@testOauth');
});

$router->group(['prefix'=>'mail'], function () use ($router) {
    $router->post('/sendCommuterMileageEmail', 'MailController@sendCommuterMileageEmail');
    $router->post('/sendTrackingTransitionEmail', 'MailController@sendTrackingTransitionEmail');
    $router->post('/sendPulteConfirmationEmail', 'MailController@sendPulteConfirmationEmail');
    $router->get('/sendPasswords', 'MailController@sendPasswords');
});

$router->group(['middleware' => ['client'], 'prefix' => 'oauth'], function () use ($router) {
    $router->get('getOauthClients', 'OauthController@getOauthClients');
    $router->post('updateClient', 'OauthController@updateClient');
    $router->post('addClient', 'OauthController@addClient');
    $router->post('deleteClient', 'OauthController@deleteClient');
});

$router->group(['middleware' => ['client']], function () use ($router) {
    $router->post('/validateUsername', 'AuthController@validateUsername');
    $router->post('/validatePasswordReset', 'AuthController@validatePasswordReset');
    $router->post('/submitNewPassword', 'AuthController@submitNewPassword');
    $router->post('/sendNewPasswordEmail', 'AuthController@sendNewPasswordEmail');
});


$router->post('/validateEmail', 'AuthController@validateEmail');
$router->post('/validateCode', 'AuthController@validateCode');
$router->post('/submitNewMiroutePassword', 'AuthController@submitNewMiroutePassword');
$router->get('/api/sendWelcome', 'AuthController@sendWelcome');

