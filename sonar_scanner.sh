#!/usr/bin/env bash

KEY=$1
URL=$2
SECRET=$3

/opt/sonar-scanner/bin/sonar-scanner -Dsonar.projectKey=${KEY} -Dsonar.sources=. -Dsonar.host.url=${URL} -Dsonar.login=${SECRET}

