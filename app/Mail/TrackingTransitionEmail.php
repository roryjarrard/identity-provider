<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TrackingTransitionEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $company_name;
    public $support;

    public function __construct($user, $company_name, $support)
    {
        $this->user = $user;
        $this->company_name = $company_name;
        $this->support = $support;
    }

    public function build()
    {
        $adminName = $this->support->firstName . ' ' . $this->support->lastName;
        return $this->from($this->support->email, $adminName)
                ->subject('Welcome to CarData Online')
                ->view('email.tracking_transition', [
                    'user' => $this->user,
                    'support' => $this->support,
                    'company_name' => $this->company_name
                ]);
                //->attach(public_path('docs/Mi-Route.2.0.Information.docx'));
    }
}
