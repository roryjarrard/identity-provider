<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Logger;

class PasswordResetEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $first_name;
    private $link;
    private $users;
    private $support;
    private $use_coordinator;
    private $logger;

    public function __construct($first_name, $link, $users, $support, $use_coordinator)
    {
        $this->first_name = $first_name;
        $this->link = $link;
        $this->users = $users;
        $this->support = $support;
        $this->use_coordinator = $use_coordinator;
        $this->logger = Logger::getRootLogger();
        $this->logger->info("In Mail class PasswordResetEmail,first_name:$first_name,link:$link");
    }

    public function build() {
        return $this->from($this->support->email, $this->support->firstName . ' ' . $this->support->lastName)->view('email.password_reset', [
            'first_name' => $this->first_name,
            'link' => $this->link,
            'users' => $this->users,
            'use_coordinator' => $this->use_coordinator,
            'support' => $this->support,
            'host'=>env('PASSWORD_RESET_HOST')
        ]);
    }
}
