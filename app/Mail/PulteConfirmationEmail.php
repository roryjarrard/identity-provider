<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PulteConfirmationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $pulteAdmin;
    public $cardataAdmins;
    public $confirmationDetails;


    public function __construct($pulteAdmin, $cardataAdmins, $confirmationDetails)
    {
        $this->pulteAdmin = $pulteAdmin; // User
        $this->cardataAdmins = $cardataAdmins; // User[]
        $this->confirmationDetails = $confirmationDetails; // [addedRows, terminatedRows, errorRows, toBeTerminated, deactivated, reactivated, runDate]
    }

    public function build()
    {
        $view = 'email.pulte_driver_file';
        $cc = [];
        foreach ($this->cardataAdmins as $admin) {
            $cc[] = $admin->email;
        }

        $supportName = $this->cardataAdmins[0]->first_name . ' ' . $this->cardataAdmins[0]->last_name;
        return $this->from($this->cardataAdmins[0]->email, $supportName)
            ->to($this->pulteAdmin->email, $this->pulteAdmin->first_name . ' ' . $this->pulteAdmin->last_name)
            ->cc($cc)
            ->subject('Pulte File Transfer Summary')
            ->view($view, [
                $this->pulteAdmin, $this->confirmationDetails
            ]);
    }
}
