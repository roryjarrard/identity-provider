<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $start_date;
    public $company_name;
    public $support;
    public $miroute;
    public $tracking;
    public $company_plan;
    public $link;

    public function __construct($user, $start_date, $company_name, $support, $miroute, $tracking, $company_plan, $link)
    {
        $this->user = $user;
        $this->start_date = $start_date;
        $this->company_name = $company_name;
        $this->support = $support;
        $this->miroute = $miroute;
        $this->tracking = $tracking;
        $this->company_plan = $company_plan;
        $this->link = $link;
    }

    public function build()
    {
        $view = 'email.welcome';
        if ($this->user['company_id'] == 57
            || $this->user['type'] == 'manager'
            || $this->user['type'] == 'admin') {
            $view .= '.pulte';
        } else if ($this->tracking) {
            $view .= '.tracking';
        } else if ($this->miroute || $this->company_plan != 'favr') {
            $view .= '.default';
        } else { // drivers only
            $view .= '.wm';
        }

        //$cc = env('APP_ENV') != 'prod' ? env('QA_EMAIL') : $cc;

        /**
         * - removed CC from all emails | rjarrard 2019-10-28
         */
        $supportName = $this->support->firstName . ' ' . $this->support->lastName;
        return $this->from($this->support->email, $supportName)->subject('Welcome to CarData Online')->view($view, ['user' => $this->user, 'start_date' => $this->start_date, 'company_name' => $this->company_name, 'support' => $this->support, 'cdo_url' => env('CDO_URL'), 'miroute' => $this->miroute, 'company_plan' => $this->company_plan, 'link' => $this->link]);
    }
}
