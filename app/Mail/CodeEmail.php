<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CodeEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $validation_code;

    public function __construct($user, $validation_code)
    {
        $this->user = $user;
        $this->validation_code = $validation_code;
    }

    public function build()
    {
        return $this->view('email.code', ['code' => $this->validation_code, 'user' => $this->user]);
    }
}
