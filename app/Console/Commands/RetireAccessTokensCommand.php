<?php
/**
 * Created by IntelliJ IDEA.
 * User: RoryJarrard
 * Date: 11/12/2019
 * Time: 11:31 AM
 */

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Providers\OauthServiceProvider;
use Logger;

class RetireAccessTokensCommand extends Command
{
    private $logger;

    protected $signature = 'daily:dailyAccessTokens';
    protected $description = 'Remove access tokens past their expiration';

    public function __construct()
    {
        parent::__construct();
        $this->logger = Logger::getRootLogger();
    }

    public function handle()
    {

        $date = date('Y-m-d H:i:s');
        $this->logger->info("Removing expired access tokens on " . $date);
        OauthServiceProvider::removeExpiredAccessTokens($date);
    }
}
