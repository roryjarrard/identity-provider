<?php
/**
 * Created by IntelliJ IDEA.
 * User: RoryJarrard
 * Date: 11/12/2019
 * Time: 11:31 AM
 */

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Providers\OauthServiceProvider;
use Logger;

class FindSendPasswordUsersCommand extends Command
{
    protected $signature = 'hourly:findSendPassword';
    protected $description = 'Find flagged users to send Welcome Email to';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        OauthServiceProvider::findSendPasswords();
    }
}
