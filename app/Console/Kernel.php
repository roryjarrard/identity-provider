<?php

namespace App\Console;

use App\Console\Commands\FindSendPasswordUsersCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Console\Commands\RetireAccessTokensCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        RetireAccessTokensCommand::class,
        FindSendPasswordUsersCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('daily:dailyAccessTokens')->daily();
        $schedule->command('hourly:findSendPassword')->withoutOverlapping(5)->everyThirtyMinutes();
    }

    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
