<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static find($company_id)
 */
class ClientSupport extends Model
{
    protected $table = 'cd_cardata.Client_Support';
    protected $primaryKey = 'client_support_id';

    protected $fillable = ['company_id', 'user_id', 'grouping_begin', 'grouping_end', 'support_type_id'];
}
