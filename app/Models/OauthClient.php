<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static find($company_id)
 */
class OauthClient extends Model
{
    protected $fillable = ['user_id', 'name', 'secret', 'redirect', 'personal_access_client', 'password_client', 'revoked'];
}
