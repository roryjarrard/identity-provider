<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use App\Models\ClientSupport;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasApiTokens;

    protected $table = 'AP_User';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'created_on';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    # TODO: had to make user_id fillable so we can bring new users in from CDO in AuthServiceProvider::getCDOUser
    #   - but this is bad and we need a new solution
    protected $fillable = [
        'user_id', 'company_id', 'username', 'password', 'first_name', 'last_name',
        'email', 'active', 'type', 'phone', 'isApproved', 'send_password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function findForPassport($username) {
        return $this->where('username', $username)->first();
    }

    public function accessTokens()
    {
        return $this->hasMany('App\Models\OauthAccessToken', 'user_id');
    }

    public function driverProfile()
    {
        return $this->belongsTo('App\Models\DriverProfile', 'user_id', 'user_id');
    }

    public function getSupportAttribute()
    {
        $admin_id = '';
        $lastInitial = substr($this->last_name, 0, 1);

        switch($this->type) {
            case 'driver':
                $support_type_id = $this->company->serv_driver_calls == 1 ? 3 : 6;
                break;
            case 'manager':
            case 'admin':
            case 'super':
            $support_type_id = $this->company->serv_driver_calls == 1 ? 7 : 6;
                break;
        }

        $clientSupport = ClientSupport::where(['company_id' => $this->company_id, 'support_type_id' => $support_type_id])->get();
        foreach ($clientSupport as $cs) {
            if ($lastInitial >= $cs->grouping_begin && $lastInitial <= $cs->grouping_end) {
                $admin_id = $cs->user_id;
            }
        }

        if ($admin_id == '') {
            $primarySupport = ClientSupport::where(['company_id' => $this->company_id, 'support_type_id' => 7])->first();
            $admin_id = empty($primarySupport) ? 2961 : $primarySupport->user_id;
        }

        return User::find($admin_id);
    }
}
