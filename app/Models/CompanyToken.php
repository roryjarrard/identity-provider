<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyToken extends Model {
	protected $fillable = ['company_id', 'token_id'];
}