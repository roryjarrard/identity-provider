<?php

if (!function_exists('public_path')) {
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function public_path($path = null)
    {
        return rtrim(app()->basePath('public/' . $path), '/');
    }
}

if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if (!function_exists('log4php_config_file')) {
    function log4php_config_file() {
        return app()->basePath() . '/config/log4php/config.' . env('APP_ENV') . '.xml';
    }
}

if (!function_exists('sluggify')) {
    function slugify($string) {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
    }
}
