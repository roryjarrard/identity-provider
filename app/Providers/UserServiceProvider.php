<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\User;
use App\Models\DriverProfile;
use Illuminate\Support\Facades\Mail;
use App\Mail\CodeEmail;
use App\Mail\WelcomeEmail;
use GuzzleHttp\Client;
use App\Models\PasswordReset;
use Logger;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function submitNewPassword($email, $password)
    {
        try {
            $user = User::where(['email' => $email])->first();

            if (empty($user)) {
                return [
                    'success' => false,
                    'title' => 'Unable to Update Password',
                    'message' => 'No user associated with the email ' . $email . ' found. Please try again, or contact your CSA for further assistance.',
                    'statusCode' => 400
                ];
            }

            $user->password = password_hash($password, PASSWORD_BCRYPT);
            $user->save();
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'Unable to Update Password',
                'message' => 'We are unable to update your password at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => 'success', 'username' => $user->username, 'statusCode' => 200];
    }

    public static function getUserInfo($user_id)
    {
        try {
            $user = User::find($user_id);
            $type = $user->type;
            $company_id = $type !== 'super' ? $user->company_id : null;
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'message' => 'We are unable to get user info at this time.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'userInfo' => compact('user_id', 'type', 'company_id')];
    }

    public static function getCarDataAdminInfo($admin_id)
    {
        return User::find($admin_id);
    }

    public static function sendWelcome($user_id, $company_id)
    {
        $logger = Logger::getRootLogger();

        try {
            $user = User::find($user_id);

            if (empty($user)) {
                throw new \Exception('No user associated with the user_id #' . $user_id . ' found.');
            }

            if ($user->company_id != $company_id) {
                throw new \Exception('User #' . $user_id . ' does not belong to company #' . $company_id);
            }

            $start_date = !empty($user->driverProfile) ? $user->driverProfile->start_date : date('Y-m-d');

            $miroute = $user->company->mileage_entry == 'miroute';
            $tracking = $user->company->mileage_entry == 'tracking';

            $company_plan = '';
            switch ($user->company->serv_plan) {
                case 1:
                    $company_plan = '463';
                    break;
                case 2:
                    $company_plan = 'favr';
                    break;
                case 3:
                    $company_plan = 'canada';
                    break;
                case 4:
                    $company_plan = 'consulting';
                    break;
            }

            $support = $user->support;
            $supportObj = (object)[
                'firstName' => $support->first_name,
                'lastName' => $support->last_name,
                'email' => $support->email,
                'phone' => $support->phone
            ];
            /*
             * supportType
             * userId
             * firstName
             * lastName
             * email
             * phone
             */


            $validation_code = rand(100000, 999999);

            $pr = PasswordReset::create([
                'user_id' => $user->user_id,
                'validation_code' => $validation_code,
                'reset' => false,
                'method' => 'sendWelcome'
            ]);

            $company_name = $user->company->name;
            $user = $user->toArray();
            unset($user['company']);
            $users = [$user];

            if (!env('RESTRICT_EMAIL')) {
                $cipher_method = env('CIPHER_METHOD');
                $cipher_key = env('CIPHER_KEY');
                $iv = env('CIPHER_IV');

                $method_hash = openssl_encrypt('init', $cipher_method, $cipher_key, false, $iv);
                $username_hash = openssl_encrypt($user['username'], $cipher_method, $cipher_key, false, $iv);
                $pr_hash = openssl_encrypt($pr->id, $cipher_method, $cipher_key, false, $iv);
                $strippedUsers = [];
                foreach ($users as $u) {
                    unset($u['external_id']);
                    unset($u['company_id']);
                    unset($u['email']);
                    unset($u['active']);
                    unset($u['phone']);
                    unset($u['isApproved']);
                    unset($u['created_on']);
                    $strippedUsers[] = $u;
                }
                $users_string = json_encode($strippedUsers);
                $users_hash = openssl_encrypt($users_string, $cipher_method, $cipher_key, false, $iv);

                $link = $method_hash . '::' . $username_hash . '::' . $pr_hash . '::' . $users_hash;

                $mailTo = env('APP_ENV') == 'prod' ? $user['email'] : env('QA_EMAIL');
                $logger->info("sending Welcome email with the TO email $mailTo");

                //mail-service (/sendWelcome)
                $client = new Client([
                    'verify' => false,
                    'base_uri' => env('MAIL_SERVICE_URL')
                ]);

                $headers = [
                     'Accept' => 'application/json',
                ];
            
                $url = '/identity/sendWelcome';
            
                $parameters = [
                    'user' => $user,
                    'start_date' =>$start_date,
                    'company_name' => $company_name,
                    'supportObj' => $supportObj,
                    'miroute' => $miroute,
                    'tracking' => $tracking,
                    'company_plan' => $company_plan,
                    'link' => $link,
                    'mailTo' => $mailTo
                ];

                $response = $client->get($url, [
                    'json' => $parameters,
                    'headers' => $headers,
                    'http_errors' => false,
                ]);

            }
        } catch (\Exception $e) {
            $logger->error('Unable to send Welcome Email to #'. $user_id . ' in company ' .$company_id . ': ' . $e->getMessage());
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'message' => 'Unable to send welcome email to user #' . $user_id . '. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        $logger->info("Welcome Email sent to #" . $user_id . " from company " . $company_id);
        return ['success' => true, 'message' => 'success'];
    }

    /**
     * Create a unique username for CarData/MiRoute
     *
     * @param string $firstName
     * @param string $lastName
     * @return string
     */
    protected function createUniqueUserName($firstName = '', $lastName = '')
    {
        $try = 1;
        $potentialName = substr($firstName, 0, 1) . substr($lastName, 0, 10);
        while (User::where('username', $potentialName)->first()) {
            $try++;
            $potentialName = substr($firstName, 0, 1) . $try . substr($lastName, 0, 10 - strlen($try . ''));
        }
        return strtolower($potentialName);
    }

    protected function getUserDate($userTime)
    {
        return explode(' ', $userTime)[0];
    }

    protected function sendEmail($user, $validation_code)
    {
        #$email = $user->email;
        $email = 'roryjarrard@gmail.com';
        Mail::to($email)->send(new CodeEmail($user, $validation_code));
    }
}
