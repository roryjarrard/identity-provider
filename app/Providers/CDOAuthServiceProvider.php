<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\ServiceProvider;
use Logger;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Mail;
use App\Mail\CodeEmail;
use App\Mail\PasswordResetEmail;
use phpDocumentor\Reflection\Types\Callable_;
use GuzzleHttp\Client;

class CDOAuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        /*
        $this->app['auth']->viaRequest('api', function ($request) {
            return app('auth')->setRequest($request)->user();
        });
        */
    }

    /**
     * Login method
     *
     * @param array $credentials
     * @return array
     */
    static public function login($username, $passwowrd, $userTime, $remote_addr = '',$sso)
    {
        $date = date('Y-m-d'); // server date
        $credentials = ['username'=>$username, 'password'=>$passwowrd];

        $logger = Logger::getRootLogger();

        try {
            $user = User::where('username', $username)->first();

            if (!$user) {
                $logger->info("FAIL,login,CDO,{$username},{$remote_addr},{$userTime},no such user");

                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "The username and password combination you entered doesn't match our records. Please try again.",
                    'statusCode' => 401
                ]; // Unauthorized
            }

            if ($user->active == 'inactive') {
                $logger->info("FAIL,login,CDO,{$username},{$remote_addr},{$userTime},inactive user");

                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "Your account has been inactivated. If you need assistance please contact your CSA or contact CarData Support.",
                    'statusCode' => 401
                ]; // Unauthorized
            }
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely
            $logger->info("EXCEPTION,login,CDO,{$username},{$remote_addr},{$userTime},{$e->getMessage()}");


            return [
                'success' => false,
                'title' => 'Unable to Log In',
                'message' => 'Unable to contact CarData at this time. If you need assistance please contact your CSA or contact CarData Support.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        try {
            if(!$sso) {
                if (!password_verify($passwowrd, $user->password)) {
                    throw new \Exception('Password incorrect');
                }
            }
            if ($user->type != 'service') {
                $user->accessTokens()->where('name', 'cardata-online')->delete();
            }
            $token = $user->createToken('cardata-online', ['cardata-online'])->accessToken;
        } catch (\Exception $e) {
            $logger->info("EXCEPTION,login,CDO,{$username},{$remote_addr},{$userTime},{$e->getMessage()}");

            return [
                'success' => false,
                'title' => 'Unable to Log In',
                'message' => "The username and password combination you entered doesn't match our records. Please try again, or contact your CSA if you need further assistance.",
                'origin_message' => 'Could Not Create Token: ' . $e->getMessage(),
                'statusCode' => 500
            ];
        }

        $success = true;
        $logger->info("SUCCESS,login,CDO,{$username},{$remote_addr},{$userTime},success");

        return ['success' => $success, 'message' => 'success', 'token' => $token];
    }

    /**
     * Used in CDO Password reset, may use email OR username
     * @param null $username
     * @param null $user_email
     * @return array
     */
    public static function validateUsername($use_coordinator, $username = null, $user_email = null)
    {
        $logger = Logger::getRootLogger();
        $users = [];

        try {
            if (empty($username) && empty($user_email)) {
                throw new \Exception("SHOW: A username or user email address was not provided.");
            }
            if (!empty($username)) {
                $user = User::where('username', $username)->first();
                $user->company_name = $user->company->name;
                $user = $user->toArray();
                unset($user['company']);
                $users = [$user];
                if (is_null($user)) {
                    $logger->error("in validateUsername user was null for $username");
                    throw new \Exception("SHOW: That username was not associated with any user in our records. Please try again, or contact your CSA for further assistance.");
                }
            } else {
                $users = User::where(['email'=>$user_email, 'active'=>'active'])->orderBy('username')->get();
                if (empty($users)) {
                    throw new \Exception("SHOW: That email was not associated with any active users in the system.");
                }

                $converted_users = [];
                foreach($users as $u) {
                    $u->company_name = $u->company->name;
                    unset($u->company);
                    $converted_users[] = $u->toArray();
                }
                usort($converted_users, function ($a, $b) {
                    return $a['company_name'] <=> $b['company_name'];
                });
                $users = $converted_users;
                $user = $users[0];
            }

            if ($user['active'] !== 'active') {
                $logger->error("in validateUsername user was inactive for $username");
                throw new \Exception('SHOW: That user is currently inactive. Please try again, or contact your CSA for further assistance.');
            }

            $validation_code = rand(100000, 999999);

            $pr = PasswordReset::create([
                'user_id' => $user['user_id'],
                'validation_code' => $validation_code,
                'reset' => false,
                'method' => 'validateUsername'
            ]);

            // self::sendEmail($user, $validation_code); changed to skip "verification code" step
            $link = self::sendPasswordResetEmail($user, $pr->id, $use_coordinator, $users, 'reset');

        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 || !is_numeric($e->getCode()) ? 500 : $e->getCode(); // mysql errors most likely

            $origin_message = $e->getMessage();
            $logger->error("in validateUsername CATCH: {$origin_message}");
            $message = substr($origin_message, 0, 5) === 'SHOW:' ? substr($origin_message, 6) : 'That username was not associated with any user in our records. Please try again, or contact your CSA for further assistance.';

            return [
                'success' => false,
                'message' => $message,
                'origin_message' => $origin_message,
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        $logger->info("password reset for {$user['user_id']}");
        return ['success' => true, 'message' => "success", 'link' => $link];
    }

    public static function validatePasswordReset($username, $pr_id)
    {
        try {
            $user = User::where('username', $username)->first();
            if (empty($user)) {
                throw new \Exception("SHOW: That username was not associated with any user in our records. Please try again, or contact your CSA for further assistance.");
            }

            $passwordReset = PasswordReset::where(['user_id' => $user->user_id, 'id' => $pr_id])->first();
            if (empty($passwordReset)) {
                throw new \Exception('SHOW: That reset link does not match our records or has expired. Please try again, or contact your CSA for further assistance.');
            }

            // Temporarily suspending expiration date - rjarrard 2019-12-19
            /*if (strtotime($passwordReset->created_at) < strtotime("-5 days")) {
                throw new \Exception('SHOW: That reset link has expired. Please request a new one, or contact your CSA for further assistance.');
            }*/
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 || !is_numeric($e->getCode()) ? 500 : $e->getCode(); // mysql errors most likely

            $origin_message = $e->getMessage();
            $message = substr($origin_message, 0, 5) === 'SHOW:' ? substr($origin_message, 6) : 'That reset link does not match our records or has expired. Please try again, or contact your CSA for further assistance.';

            return [
                'success' => false,
                'message' => $message,
                'origin_message' => $origin_message,
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message'=>'success', 'user_id' => $user->user_id];
    }

    public static function sendNewPasswordEmail($pr_id)
    {
        $logger = Logger::getRootLogger();

        try {
            $pr = PasswordReset::find($pr_id);

            if (empty($pr)) {
                throw new \Exception('No reset record found');
            }

            $user = User::find($pr->user_id);
            if ($user->active != 'active') {
                throw new \Exception('Driver is inactive');
            }

            if (empty($pr->method)) {
                self::validateUsername(false, $user->username, $user->user_email);
            }
        } catch (\Exception $e) {
            $logger->error('Could not resend password reset: ' . $e->getMessage());

            $statusCode = $e->getCode() >= 1000 || !is_numeric($e->getCode()) ? 500 : $e->getCode(); // mysql errors most likely
            return [
                'success' => false,
                'message' => 'We are unable to resend your password email at this time. Please try again, or contact your CSA for further assistance',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => 'New email sent'];
    }

    public static function submitNewPassword($user_id, $new_password, $password_verify)
    {
        try {
            $user = User::find($user_id);

            if (empty($user)) {
                throw new \Exception('SHOW: That user id was not associated with any user in our records. ');
            }

            if ($new_password !== $password_verify) {
                throw new \Exception('SHOW: Those passwords do not match. Please type them again and resubmit. If you need assistance please contact your CSA.');
            }

            $user->update(['password' => password_hash($new_password, PASSWORD_BCRYPT)]);
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 || !is_numeric($e->getCode()) ? 500 : $e->getCode(); // mysql errors most likely

            $origin_message = $e->getMessage();
            $message = substr($origin_message, 0, 5) === 'SHOW:' ? substr($origin_message, 6) : 'We were unable to reset your password at this time. Please try again, or contact your CSA for further assistance.';

            return [
                'success' => false,
                'message' => $message,
                'origin_message' => $origin_message,
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => 'success'];
    }

    protected static function sendPasswordResetEmail($user, $pr_id, $use_coordinator, $users, $method)
    {
        if (env('RESTRICT_EMAIL')) {
            return;
        }

        $support = User::find($user['user_id'])->support;
        $supportObj = (object)[
            'firstName' => $support->first_name,
            'lastName' => $support->last_name,
            'email' => $support->email,
            'phone' => $support->phone
        ];

        $email = env('APP_ENV') === 'prod' ? $user['email'] : env('QA_EMAIL');

        if (!empty($email)) {
            $cipher_method = env('CIPHER_METHOD');
            $cipher_key = env('CIPHER_KEY');
            $iv = env('CIPHER_IV');
            $method_hash = openssl_encrypt($method, $cipher_method, $cipher_key, false, $iv);
            $username_hash = openssl_encrypt($user['username'], $cipher_method, $cipher_key, false, $iv);
            $pr_hash = openssl_encrypt($pr_id, $cipher_method, $cipher_key, false, $iv);
            $strippedUsers = [];
            foreach ($users as $u) {
                unset($u['external_id']);
                unset($u['company_id']);
                unset($u['email']);
                unset($u['active']);
                unset($u['phone']);
                unset($u['isApproved']);
                unset($u['created_on']);
                $strippedUsers[] = $u;
            }
            $users_string = json_encode($strippedUsers);
            $users_hash = openssl_encrypt($users_string, $cipher_method, $cipher_key, false, $iv);

            $link = $method_hash . '::' . $username_hash . '::' . $pr_hash . '::' . $users_hash;

            try {
                //mail-service (/sendPaswordResetEmail)
                $client = new Client([
                    'verify' => false,
                    'base_uri' => env('MAIL_SERVICE_URL')
                ]);
    
                $headers = [
                    'Accept' => 'application/json',
                ];
        
                $url = '/identity/sendPasswordResetEmail';
        
                $parameters = [
                    'user' => $user,
                    'link' => $link,
                    'users' => $users,
                    'supportObj' => $supportObj,
                    'use_coordinator' => $use_coordinator,
                    'email' => $email
                ];
    
                
                $response = $client->post($url, [
                    'json' => $parameters,
                    'headers' => $headers,
                    'http_errors' => false,
                ]);

                
    
            } catch (\Exception $e) {
                $logger->error('Unable to send password reset email to '. $email . ': ' . $e->getMessage());
                $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode();
    
                return [
                    'success' => false,
                    'message' => 'Unable to send password reset email to ' . $email . '. Please try again, or contact your CSA for further assistance.',
                    'origin_message' => $e->getMessage(),
                    'origin_status' => $e->getCode(),
                    'statusCode' => $statusCode
                ];
            }

            return response()->json(['message' => 'password reset email sent'], 200);
        }
    }

    protected static function sendEmail($user, $validation_code)
    {
        if (env('RESTRICT_EMAIL')) {
            return;
        }

        $email = $_SERVER['SERVER_ADDR'] === '209.251.54.134' ? $user->email : env('QA_EMAIL');


        if (!empty($email)) {
            Mail::to($email)->send(new CodeEmail($user, $validation_code));
        }
    }
}
