<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
        Passport::tokensCan([
            'miroute-api' => 'Utilize Mi-Route App',
            'cardata-online' => 'Use CarData Online'
        ]);
        Passport::personalAccessTokensExpireIn(Carbon::now()->addDays(env('PASSPORT_EXP_DAYS')));
    }
}
