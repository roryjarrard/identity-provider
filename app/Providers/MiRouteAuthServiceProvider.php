<?php

namespace App\Providers;

use Illuminate\Support\Facades\Mail;
use App\Mail\CodeEmail;
use App\Models\User;
use App\Models\PasswordReset;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Hash;
use Logger;
use GuzzleHttp\Client;

class MiRouteAuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.



        $this->app['auth']->viaRequest('api', function ($request) {
            /*if ($request->input('api_token')) {
                return User::where('api_token', $request->input('api_token'))->first();
            }*/
            return app('auth')->setRequest($request)->user();
        });
    }

    /**
     * Login method
     *
     * @param $username
     * @param $password
     * @param $userTime
     * @param string $remote_addr
     * @return array
     */
    static public function login($username, $password, $userTime, $remote_addr = 'unknown')
    {
        $date = date('Y-m-d'); // server date
        $credentials = ['username' => $username, 'password' => $password];

        $logger = Logger::getRootLogger();

        try {

            $userCollection = User::where('username', $username);

            if (empty($userCollection)) {
                $logger->info("FAIL,login,Mi-Route,{$username},{$remote_addr},{$userTime},no such user");
                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "The username and password combination you entered doesn't match our records. Please try again.",
                    'statusCode' => 401
                ]; // Unauthorized
            }

            $user = $userCollection->first();
            if (empty($user)) {
                $logger->info("FAIL,login,Mi-Route,{$username},{$remote_addr},{$userTime},no such user");
                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "The username and password combination you entered doesn't match our records. Please try again.",
                    'statusCode' => 401
                ]; // Unauthorized
            }

            if ($user->active == 'inactive') {

                $logger->info("FAIL,login,Mi-Route,{$username},{$remote_addr},{$userTime},user inactive");

                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "Your account has been inactivated. If you need assistance please contact your CSA or contact CarData Support.",
                    'statusCode' => 401
                ]; // Unauthorized
            }
            if ($user->type !== 'driver') {
                $logger->info("FAIL,login,Mi-Route,{$username},{$remote_addr},{$userTime},user not a driver");
                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "You are not allowed to use Mi-Route. Please use <a href='https://www.cardataconsultants.com'>www.cardataconsultants.com</a> instead.",
                    'statusCode' => 401
                ]; // Unauthorized
            }

            $client = new Client([
                'verify' => false,
                'base_uri' => env('RULES_URL')
            ]);


            try {
                $response = $client->get('/auth/loginEligibility/' . $user->user_id);
                $responseBody = json_decode($response->getBody());
            } catch (\Exception $e) {
                $statusCode = $e->getCode() >= 1000 || !is_numeric($e->getCode()) ? 500 : $e->getCode(); // mysql errors most likely

                $logger->info("EXCEPTION,mirouteLogin,{$username},guzzle exception: {$e->getMessage()}");

                return [
                    'success' => false,
                    'title' => $response->title,
                    'message' => $response->message,
                    'origin_message' => $response->origin_message,
                    'origin_status' => $response->origin_status,
                    'statusCode' => $statusCode
                ];
            }

            if (!$responseBody->is_eligible) {
                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "You are not allowed to use Mi-Route. Please use <a href='https://www.cardataconsultants.com'>www.cardataconsultants.com</a> instead.",
                    'origin_message' => $responseBody->message,
                    'origin_status' => 401,
                    'statusCode' => 401
                ];
            }
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely
            $logger->info("EXCEPTION,login,Mi-Route,{$username},{$remote_addr},{$userTime},{$e->getMessage()}");

            return [
                'success' => false,
                'title' => 'Unable to Log In',
                'message' => 'Unable to contact CarData at this time. If you need assistance please contact your CSA or contact CarData Support.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        try {
            if (!password_verify($password, $user->password)) {
                throw new \Exception('Password incorrect');
            }
            $user->accessTokens()->where('name', 'miroute-api')->delete();
            $token = $user->createToken('miroute-api', ['miroute-api'])->accessToken;
        } catch (\Exception $e) {
            $logger->info("EXCEPTION,login,Mi-Route,{$username},{$remote_addr},{$userTime},{$e->getMessage()}");

            return [
                'success' => false,
                'title' => 'Unable to Log In',
                'message' => "The username and password combination you entered doesn't match our records. Please try again, or contact your CSA if you need further assistance.",
                'origin_message' => 'Could Not Create Token: ' . $e->getMessage(),
                'statusCode' => 401
            ];
        }

        // generates a permanent token for tracking users
        try {
            $tracker_token = self::getPermanentToken($user->user_id);
            if (!$tracker_token) {
                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "The username and password combination you entered doesn't match our records. Please try again.",
                    'statusCode' => 401
                ]; // Unauthorized
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'title' => 'Unable to Log In',
                'message' => "The username and password combination you entered doesn't match our records. Please try again, or contact your CSA if you need further assistance.",
                'origin_message' => 'Could Not Create Permanent Token: ' . $e->getMessage(),
                'statusCode' => 500
            ];
        }

        # TODO: Log to MILogin
        $success = true;
        $logger->info("SUCCESS,login,Mi-Route,{$username},{$remote_addr},{$userTime},success");

        return ['success' => $success, 'message' => 'success', 'token' => $token, 'tracker_token' => $tracker_token];
    }

    public static function validateEmail($email)
    {
        try {
            $user = User::where(['email' => $email, 'type'=>'driver'])->first();
            if (is_null($user)) {
                throw new \Exception("SHOW: That username was not associated with any user in our records. Please try again, or contact your CSA for further assistance.");
            }

            if ($user->active !== 'active') {
                throw new \Exception('SHOW: That user is currently inactive. Please try again, or contact your CSA for further assistance.');
            }

            $validation_code = rand(100000, 999999);

            $pr = PasswordReset::create([
                'user_id' => $user->user_id,
                'validation_code' => $validation_code,
                'reset' => false,
                'method' => 'validateEmail'
            ]);

            self::sendCodeEmail($user, $validation_code);
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 || !is_numeric($e->getCode()) ? 500 : $e->getCode(); // mysql errors most likely

            $origin_message = $e->getMessage();
            $message = substr($origin_message, 0, 5) === 'SHOW:' ? substr($origin_message, 6) : 'That username was not associated with any user in our records. Please try again, or contact your CSA for further assistance.';

            return [
                'success' => false,
                'message' => $message,
                'origin_message' => $origin_message,
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => "email sent"];
    }


    public static function validateCode($email, $code)
    {
        try {
            $user = User::where(['email' => $email, 'type' => 'driver'])->first();
            if (empty($user)) {
                return [
                    'success' => false,
                    'title' => 'Unable to Validate Code',
                    'message' => 'No validation code associated with the email ' . $email . ' found. Please try again, or contact your CSA for further assistance.',
                    'statusCode' => 400
                ];
            }

            $passwordReset = PasswordReset::where(['user_id' => $user->user_id, 'validation_code' => $code])->orderBy('created_at', 'desc')->first();

            if (empty($passwordReset)) {
                return [
                    'success' => false,
                    'title' => 'Unable to Validate Code',
                    'message' => 'No validation code associated with the email ' . $email . ' found. Please try again, or contact your CSA for further assistance.',
                    'statusCode' => 400
                ];
            } elseif (strtotime($passwordReset->created_at) < strtotime("-5 days")) {
                return [
                    'success' => false,
                    'title' => 'Unable to Validate Code',
                    'message' => 'That validation code has expired. Please submit your email for a new validation code.',
                    'statusCode' => 400
                ];
            }

            $passwordReset->reset = 1;
            $passwordReset->save();

        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 || !is_numeric($e->getCode()) ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'Unable to Validate Code',
                'message' => 'That code did not exist or has expired. Please try again or have a new code sent.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => 'success', 'statusCode' => 200];
    }

    public static function submitNewPassword($email, $new_password)
    {
        try {
            $user = User::where(['email'=>$email, 'type'=>'driver'])->first();

            if (empty($user)) {
                throw new \Exception('SHOW: That user id was not associated with any user in our records. ');
            }

            $user->update(['password' => password_hash($new_password, PASSWORD_BCRYPT)]);
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 || !is_numeric($e->getCode()) ? 500 : $e->getCode(); // mysql errors most likely

            $origin_message = $e->getMessage();
            $message = substr($origin_message, 0, 5) === 'SHOW:' ? substr($origin_message, 6) : 'We were unable to reset your password at this time. Please try again, or contact your CSA for further assistance.';

            return [
                'success' => false,
                'message' => $message,
                'origin_message' => $origin_message,
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => 'success', 'username' => $user->username];
    }

    protected static function sendCodeEmail($user, $validation_code)
    {
        if (env('RESTRICT_EMAIL')) {
             return;
        }

        /**
         * CCS-1985
         * switched production to prod
         */
        $email = env('APP_ENV') === 'prod' ? $user->email : env('QA_EMAIL');

        //mail-service (/sendCodeEmail)
        if (!empty($email)) {
            try {
                $client = new Client([
                    'verify' => false,
                    'base_uri' => env('MAIL_SERVICE_URL')
                ]);
    
                $headers = [
                    'Accept' => 'application/json',
                ];
        
                $url = '/identity/sendCodeEmail';
        
                $parameters = [
                    'user' => $user,
                    'validation_code' =>$validation_code,
                    'email' => $email
                ];
    
                $response = $client->post($url, [
                    'json' => $parameters,
                    'headers' => $headers,
                    'http_errors' => false,
                ]);
    
            } catch (\Exception $e) {
                $logger->error('Unable to send validation code email to '. $email . ': ' . $e->getMessage());
                $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode();
    
                return [
                    'success' => false,
                    'message' => 'Unable to send validation code email to ' . $email . '. Please try again, or contact your CSA for further assistance.',
                    'origin_message' => $e->getMessage(),
                    'origin_status' => $e->getCode(),
                    'statusCode' => $statusCode
                ];
            }

        }
        
    }


    /**
     * Creates a hash (with salt this is non-deterministic)
     * from a user's user_id
     *
     * This will authorize our tracker, so we do not
     * have to worry about token expiry.
     *
     * Middleware will handle the deactivation!!
     *
     * @param string $username
     * @param int $company_id
     * @return string
     */
    static protected function getPermanentToken(int $user_id)
    {
        /**
         * CCS-1917
         * The trim is required here as we trim in other places.
         * We had issues where the hashes did not match and locations
         * were not being sent to the server.
         */
        $plaintext = trim($user_id) . env("TRACKING_SECRET");
        return Hash::make($plaintext);
    }
}
