<?php

namespace App\Providers;

use App\Models\Company;
use App\Models\CompanyToken;
use App\Models\OauthAccessToken;
use App\Models\OauthClient;
use App\Models\User;
use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Logger;

class OauthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function getOauthClients($user)
    {
        $logger = Logger::getRootLogger();
        $clients = [];
        try {
            $clients = OauthClient::all();
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely
            $logger->info("EXCEPTION,getOauthClients,{$user->user_id},{$user->username},{$e->getMessage()}");


            return [
                'success' => false,
                'message' => 'We were unable to get Oauth clients at this time. Please try again.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }
        return ['success' => true, 'message' => 'success', 'clients' => $clients];
    }

    public static function updateClient($user, $client)
    {
        $logger = Logger::getRootLogger();
        try {
            $clients = OauthClient::all();

            foreach ($clients as $c) {
                if ($c->id != $client['id'] && $client['name'] == $c->name) {
                    throw new \Exception("That name is already in use.");
                }
            }
            $client = OauthClient::find($client['id'])->update($client);
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely
            $logger->info("EXCEPTION,updateClient,{$user->user_id},{$user->username},{$e->getMessage()}");


            return [
                'success' => false,
                'message' => 'We were unable to update your client at this time. Please try again.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message'=>'success'];
    }

    public static function addClient($user, $client_name)
    {
        $logger = Logger::getRootLogger();
        try {
            $clients = OauthClient::all();

            foreach ($clients as $c) {
                if ($client_name == $c->name) {
                    $logger->error($client_name . " is already in use");
                    return [
                        'success' => false,
                        'message' => 'We were unable to add your client at this time. Please try again.',
                        'origin_message' => $client_name . " is already in use",
                        'origin_status' => '',
                        'statusCode' => 500
                    ];
                }
            }

            $client = new Client([
                'verify' => false,
                'base_uri' => env('APP_URL')
            ]);
            $headers = [
                'Authorization' => $_SERVER['HTTP_AUTHORIZATION'],
                'Accept' => 'application/json'
            ];
            $response = $client->post('/api/oauth/clients', [
                'headers' => $headers,
                RequestOptions::JSON => ['name'=>$client_name, 'redirect'=>env('APP_URL')]
            ]);

            $responseBody = json_decode($response->getBody());
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely
            $logger->error("EXCEPTION,updateClient,{$user->user_id},{$user->username},{$e->getMessage()}");

            return [
                'success' => false,
                'message' => 'We were unable to add your client at this time. Please try again.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message'=>'success', 'secret' => $responseBody->secret, 'id'=>$responseBody->id];
    }

    public static function addClientById($user, $companyId)
    {
        $logger = Logger::getRootLogger();

        $company = Company::where('company_id',$companyId)->first();

        $response = self::addClient($user,$company->name);
        $logger->info($response);
        if($response['success']) {
            $companyToken = new CompanyToken();
            $companyToken->company_id = $companyId;
            $companyToken->token_id = $response['id'];
            $companyToken->save();
        }

        return $response;
    }

    public static function deleteClient($user, $client_id)
    {
        $logger = Logger::getRootLogger();
        try {
            $client = OauthClient::find($client_id);

            if (empty($client)) {
                throw new \Exception("That client does not exist");
            }

            OauthClient::destroy($client_id);

        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely
            $logger->info("EXCEPTION,updateClient,{$user->user_id},{$user->username},{$e->getMessage()}");

            return [
                'success' => false,
                'message' => 'We were unable to delete your client at this time. Please try again.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message'=>'success'];
    }

    public static function removeExpiredAccessTokens($date)
    {
        $logger = Logger::getRootLogger();
        $accessTokens = OauthAccessToken::where('expires_at', '<', $date)->get();
        $logger->info("Removing " . $accessTokens->count() . " access tokens on " . $date);
        OauthAccessToken::where('expires_at', '<', $date)->delete();
    }

    public static function findSendPasswords()
    {
        $logger = Logger::getRootLogger();
        $logger->info("in findSendPasswords");
        $flaggedUsers = User::where('send_password', 1)->get();

        if (count($flaggedUsers)) {
            $logger->info("Sending welcome to " . count($flaggedUsers) . " users");

            foreach ($flaggedUsers as $user) {
                UserServiceProvider::sendWelcome($user->user_id, $user->company_id);
                $user->send_password = 0;
                $user->save();
            }
        }
    }
}
