<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Providers\UserServiceProvider;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Providers\MiRouteAuthServiceProvider;
use App\Providers\CDOAuthServiceProvider;
use Logger;
use App\Models\OauthAccessToken;
use App\Models\OauthClient;
use App\Models\CompanyToken;
use Lcobucci\JWT\Parser;

class AuthController extends Controller
{
    private $logger;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->logger = Logger::getRootLogger();
    }

    /**
     * Authenticate with username and password
     * - user must be a driver
     * - user must be active
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $userTime = $this->getUserTime($request->get('userTime'));
        $loginApp = $this->determineAppName($request->get('loginapp'));
        $remote_addr = $this->getUserAddress($request->get('remote_addr'));
        $sso = $this->determineSSO($request->get('sso'));

        $username = $request->get('username');
        $password = $request->get('password');

        $this->logger->info("Attempting login through {$loginApp} for {$username}");
        try {
            if($loginApp === 'miroute') {
                $response = MiRouteAuthServiceProvider::login($username, $password, $userTime, $remote_addr);
            }else if($loginApp === 'cdo') {
                $response = CDOAuthServiceProvider::login($username, $password, $userTime, $remote_addr,$sso);
            }

        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Log In',
                'message' => 'Unable to contact CarData at this time. If you need assistance please contact your CSA or contact CarData Support.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response) && isset($response['success']) && $response['success']) {
            $tracker_token = isset($response['tracker_token']) ? $response['tracker_token'] : null;
            return response()->json([
                'message' => $response['message'],
                'token' => $response['token'],
                'tracker_token' => $tracker_token
            ], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    public function logout(Request $request)
    {
        try {
            $user_id = $request->get('user_id');
            $scope = $request->get('scope');
            $user = User::find($user_id);

            if ($user->type != 'service') {
                $user->accessTokens()->where('name', $scope)->delete();
            }
            return response()->json(['message' => 'logged out of ' . $scope], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'status' => $e->getCode()], 500);
        }
    }

    /**
     * External call to delete tokens on logout
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteAccessTokens(Request $request)
    {
      try {
          $user_id = $request->get('user_id');
          $scope = $request->get('scope');
          User::find($user_id)->accessTokens()->where('name', $scope)->delete();
          return response()->json(['message' => 'success'], 200);
      } catch (\Exception $e) {
        return response()->json(['message' => $e->getMessage(), 'status' => $e->getCode()], 500);
      }
    }

    public function sendWelcome(Request $request)
    {
        $user_id = $request->get('user_id');
        $company_id = $request->get('company_id');

        try {
            $response = UserServiceProvider::sendWelcome($user_id, $company_id);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Unable to send welcome email to user #'.$user_id.'. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    public function validateUsername(Request $request)
    {
        try {
            $use_coordinator = $request->has('use_coordinator') ? $request->get('use_coordinator') : false;
            $response = CDOAuthServiceProvider::validateUsername($use_coordinator, $request->get('username'), $request->get('user_email'));
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'That email was not associated with any user in our records. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => 'success', 'link' => $response['link']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    /*
     * Used from Miroute Api
     */
    public function validateEmail(Request $request)
    {
        try {
            $response = MiRouteAuthServiceProvider::validateEmail($request->get('email'));
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'That email was not associated with any user in our records. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['success' => true, 'message' => $response['message']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    /**
     * Validate submitted code to then allow reset password
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function validateCode(Request $request)
    {
        try {
            $response = MiRouteAuthServiceProvider::validateCode($request->get('email'), $request->get('code'));
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Validate Code',
                'message' => 'We are unable to validate your code at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['success' => true, 'message' => $response['message']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode,
            ], $statusCode);
        }
    }

    public function validatePasswordReset(Request $request)
    {
        $username = $request->get('username');
        $pr_id = $request->get('pr_id');

        try {
            $response = CDOAuthServiceProvider::validatePasswordReset($username, $pr_id);
        } catch (\Exception $e) {
            return response() ->json([
                'message' => 'That reset link does not match our records or has expired. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message'], 'user_id' => $response['user_id']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    public function submitNewPassword(Request $request)
    {
        $user_id = $request->get('user_id');
        $new_password = $request->get('new_password');
        $password_verify = $request->get('password_verify');

        try {
            $response = CDOAuthServiceProvider::submitNewPassword($user_id, $new_password, $password_verify);
        } catch (\Exception $e) {
            return response() ->json([
                'message' => 'We were unable to reset your password at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    public function submitNewMiroutePassword(Request $request)
    {
        $email = $request->get('email');
        $new_password = $request->get('password');

        try {
            $response = MiRouteAuthServiceProvider::submitNewPassword($email, $new_password);
        } catch (\Exception $e) {
            return response() ->json([
                'message' => 'We were unable to reset your password at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['success' => true, 'message' => $response['message'], 'username' => $response['username']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    public function sendNewPasswordEmail(Request $request)
    {
        try {
            $response = CDOAuthServiceProvider::sendNewPasswordEmail($request->get('pr_id'));
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'We are unable to resend your password email at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['success' => true, 'message' => $response['message']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    public function getCarDataAdminInfo(Request $request)
    {
        $admin_id = $request->get('cardata_admin_id');
        return UserServiceProvider::getCarDataAdminInfo($admin_id);
    }

    public function getUserInfo(Request $request, $driver_id = null) {
        $user_id = !empty($driver_id) ? $driver_id : $request->user()->user_id;
        try {
            $response = UserServiceProvider::getUserInfo($user_id);
        } catch (\Exception $e) {
            return response() ->json([
                'message' => 'We were unable to get user information at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['userInfo'=>$response['userInfo']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    public function getClientInfo(Request $request) {
        try {
            $bearerToken = $request->bearerToken();
            $tokenInfo = (new Parser())->parse($bearerToken);
            $tokenId = $tokenInfo->getHeader('jti');
            $oauthAccess = OauthAccessToken::find($tokenId);
            $client = OauthClient::find($oauthAccess->client_id);
            if(!is_null($client)) {
                $companyToken = CompanyToken::where('token_id', '=', $client->id)->firstOrFail();
                $returnValue = [
                    "id"=>$client->id
                    ,"company_id"=>$companyToken->company_id
                ];
                return response()->json(['client'=>$returnValue], 200);
            }else {
                return response()->json(['client'=>$client], 404);
            }
        }catch(\Exception $e) {
            $this->logger->error($e);
            return response()->json(['Error in getClientInfo',$e->getMessage()], 500);
        }
    }

    public function testOauth(Request $request)
    {
        return response()->json(['message'=>"I hear you " . $request->user()->first_name], 200);
    }

    protected function getUserTime($userTime)
    {
        return (!empty($userTime) ? $userTime : date('Y-m-d H:i:s'));
    }

    protected function determineAppName($appName)
    {
        return (!empty($appName) ? $appName : 'miroute');
    }

    protected function determineSSO($sso) {
        return (!empty($sso) ? $sso : false);
    }

    protected function getUserAddress($remote_address)
    {
        return (!empty($remote_address) ? $remote_address : 'unknown');
    }
}
