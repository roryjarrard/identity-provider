<?php

namespace App\Http\Controllers;

use App\Mail\CommuterMileageEmail;
use App\Mail\PulteConfirmationEmail;
use App\Mail\TrackingTransitionEmail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Client;
use Logger;

class MailController extends Controller
{
    private $logger;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->logger = Logger::getRootLogger();
    }

    public function sendCommuterMileageEmail(Request $request)
    {
        try {
            $emailTo = $request->get('email');
            $subject = $request->get('subject');
            $message = $request->get('message');

            //mail-service (/sendCommuterMileageEmail)
            $client = new Client([
                'verify' => false,
                'base_uri' => env('MAIL_SERVICE_URL')
            ]);

            $headers = [
                 'Accept' => 'application/json',
            ];
        
            $url = '/identity/sendCommuterMileageEmail';
            
            $parameters = [
                'emailTo' => $emailTo,
                'subject' => $subject,
                'message' => $message
            ];
            
            $response = $client->post($url, [
                'json' => $parameters,
                'headers' => $headers,
                'http_errors' => false,
            ]);

        } catch (\Exception $e) {
            $this->logger->error('Unable to send commuter mileage email to ' . $email . ': ' . $e->getMessage());
            $statusCode = $e->getCode() >= 600 ? 500 : $e->getCode();
            return response()->json(['message' => 'ERROR: sendCommuterMileageEmail ' . $e->getMessage()], $statusCode);
        }

        return response()->json(['success' => true, 'message' => 'success'], 200);
    }

    public function sendTrackingTransitionEmail(Request $request)
    {
        try {
            $user = User::find($request->get('user_id'));
            $token = $user->createToken('cardata-online', ['cardata-online']);
            $email = getenv('APP_ENV') == 'prod' ? $user->email : getenv('QA_EMAIL');

            $support = $user->support;
            $supportObj = (object)[
                'firstName' => $support->first_name,
                'lastName' => $support->last_name,
                'email' => $support->email,
                'phone' => $support->phone
            ];

            //mail-service (/sendTrackingTransitionEmail)
            $client = new Client([
                'verify' => false,
                'base_uri' => env('MAIL_SERVICE_URL')
            ]);

            $headers = [
                 'Accept' => 'application/json',
            ];
        
            $url = '/identity/sendTrackingTransitionEmail';
            
            $parameters = [
                'user' => $user,
                'company_name' => $user->company->name,
                'supportObj' => $supportObj,
                'email' => $email
            ];
            
            $response = $client->post($url, [
                'json' => $parameters,
                'headers' => $headers,
                'http_errors' => false,
            ]);

        } catch (\Exception $e) {
            $this->logger->error('Unable to send tracking transition email to ' . $email . ': ' . $e->getMessage());
            $statusCode = $e->getCode() >= 600 ? 500 : $e->getCode();
            return response()->json(['message' => 'ERROR: sendTrackingTransitionEmail ' . $e->getMessage()], $statusCode);
        }
        
        return response()->json(['success' => true, 'message' => 'success'], 200);
    }

    public function sendPulteConfirmationEmail(Request $request)
    {
        try {
            $pulteAdmin = User::find($request->get('company_admin_id'));
            $cardataAdmins = [];
            foreach ($request->get('cardata_admin_ids') as $admin_id) {
                $cardataAdmins[] = User::find($admin_id);
            }

            $confirmationData = $request->all();

            //mail-service (/sendPulteConfirmationEmail)
            $client = new Client([
                'verify' => false,
                'base_uri' => env('MAIL_SERVICE_URL')
            ]);

            $headers = [
                 'Accept' => 'application/json',
            ];
        
            $url = '/identity/sendPulteConfirmationEmail';
            
            $parameters = [
                'pulteAdmin' => $pulteAdmin,
                'cardataAdmins' => $cardataAdmins,
                'confirmationData' => $confirmationData
            ];
            
            $response = $client->post($url, [
                'json' => $parameters,
                'headers' => $headers,
                'http_errors' => false,
            ]);

        } catch (\Exception $e) {
            $this->logger->error("Error sending Pulte confirmation email: " . $e->getMessage());
            $statusCode = $e->getCode() >= 600 ? 500 : $e->getCode();
            return response()->json(['error' => 'sending Pulte confirmation email: ' . $e->getMessage()], $statusCode);
        }

        return response()->json(['success' => true, 'message' => 'success'], 200);
    }
}
