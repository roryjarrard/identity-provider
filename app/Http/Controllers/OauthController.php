<?php

namespace App\Http\Controllers;

use App\Providers\OauthServiceProvider;
use Illuminate\Http\Request;
use Logger;

class OauthController extends Controller
{
    private $logger;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->logger = Logger::getRootLogger();
    }

    public function getOauthClients(Request $request)
    {
        try {
            $response = OauthServiceProvider::getOauthClients($request->user());
        } catch (\Exception $e) {
            return response() ->json([
                'message' => 'We were unable to get Oauth clients at this time. Please try again.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if ($response['success']) {
            return response()->json(['message'=>$response['message'], 'clients' => $response['clients']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    public function updateClient(Request $request)
    {
        try {
            $response = OauthServiceProvider::updateClient($request->user(), $request->get('client'));
        } catch (\Exception $e) {
            return response() ->json([
                'message' => 'We were unable to update your client at this time. Please try again.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if ($response['success']) {
            return response()->json(['message'=>$response['message']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    public function addClient(Request $request)
    {
        try {
            if(!is_null($request->get('client_name'))) {
                $response = OauthServiceProvider::addClient($request->user(), $request->get('client_name'));
            }else if(!is_null($request->get('client_id'))) {
                $response = OauthServiceProvider::addClientById($request->user(), $request->get('client_id'));
            }
        } catch (\Exception $e) {
            return response() ->json([
                'message' => 'We were unable to add your client at this time. Please try again.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if ($response['success']) {
            return response()->json(['message'=>$response['message'], 'id'=>$response['id'], 'secret'=>$response['secret']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    public function deleteClient(Request $request)
    {
        try {
            $response = OauthServiceProvider::deleteClient($request->user(), $request->get('client_id'));
        } catch (\Exception $e) {
            return response() ->json([
                'message' => 'We were unable to delete your client at this time. Please try again.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if ($response['success']) {
            return response()->json(['message'=>$response['message']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }
}
