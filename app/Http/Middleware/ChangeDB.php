<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Models\Company;
use App\Models\User;

class ChangeDB
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $username = $request->get('username');
            $user = User::where('username', $username)->first();

            if (is_null($user)) {
                return response()->json([
                    'title' => 'Unable to Log In',
                    'message' => "The username and password combination you entered doesn't match our records. Please try again.",
                    'origin_message' => "The username and password combination you entered doesn't match our records. Please try again.",
                    'origin_status' => 401, 
                    'status_code' => 401
                ], 401);
            }
            $company = Company::find($user->company_id);
            $db = $company->database_name;
            if (is_null($db)) {
                $db = env('DEFAULT_DB');
            }

            Config::set('database.connections.cardata.database', $db);
            DB::reconnect('cardata');

        } catch (Exception $e) {
            return response()->json(['message' => $e], 500);
        }
        return $next($request);
    }
}
